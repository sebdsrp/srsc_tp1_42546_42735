# README
#### 1. Preparação dos documentos necessários
##### users.conf
# 
password: password_<USERNAME>
# 
<USERNAME>/<USERNAME>@fct.unl.pt: <SHA512(password)>
# 
e.g.:
```sh
seb/seb@fct.unl.pt: f31d8dcb861f1d18d44bf4699a140b62a48851eab9dee060789142dc3d5f841a66e2ff6932f52b908800454775f7d3c5143f8b71456540d88120394c942ee3a7
maria/maria@fct.unl.pt: 86bbcc45b3cfb0b984e6317edd15774ccc89f2d466f7573a649aebd765d6119c4f2ee1612336faa71111e4c707857c385035706cdcdef6cb3abf3cfdc00d1d21
miguel/miguel@fct.unl.pt: f7264be89a52be990ca30d9992ba9b74bd2ba2bdd1e457159812fbe4db5ccf9141fc282f05cf505235ff95a7f065e186b02dff0aef6fb6a86cee71f7a37f7429
```
##### dacl.conf
# 
<IPMC>: <USERNAME>, <USERNAME>, <USERNAME> ...
# 
<IPMC>: <USERNAME>, <USERNAME>, <USERNAME> ...
# 
e.g.:
```sh
224.10.10.10: seb, maria, miguel
233.10.10.10: seb, maria
```
##### stgcsap.auth
# 
STGC-SAP: <PBEALGORITHM>:<MACALGORITHM>:
# 
e.g.:
```sh
STGC-SAP: PBEWithSHAAnd3KeyTripleDES:HMacSHA1:
```
##### ciphersuit.conf
# 
<IPMC>: <ALGORITHM><CIPHERSUIT><MACALGORITHM>
# 
e.g.:
```sh
233.10.10.10: AES AES/CBC/PKCS5Padding HMacSHA1
224.10.10.10: AES AES/CBC/PKCS5Padding HMacSHA1
```
#### Correr o programa
[1] Preparar os ficheiros, tal como demonstrado no ponto 1 (pode utilizar os exemplos)
[2] Correr a class AuthenticationServer.java
[3] Correr a class MChatClient.java com os seguintes argumentos: seb:password_seb 224.10.10.10 3000
[4] Correr a class MChatClient.java com os seguintes argumentos: maria:password_maria 224.10.10.10 3000
### Installation

### Plugins

### Development
